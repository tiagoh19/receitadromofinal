import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.border.LineBorder;
import javax.swing.JEditorPane;
import javax.swing.JCheckBox;
import javax.swing.JTextPane;
import javax.swing.JComboBox;

public class Menu extends JFrame {

	private JPanel contentPane;
	private JTextField tb_nome;
	private JTextField tb_n_doses;
	private JTextField tb_tempo_prep;
	private JTextField tb_tempo_confe;
	private JTextField tb_classi;
	private JTextField tb_observ;
	private JTextField tb_tipo_receita;
	private JTextField txt_idreceita;
	private JTextField tb_ver_nome;
	private JTextField tb_ver_n_doses;
	private JTextField tb_ver_tempo_prepa;
	private JTextField tb_ver_tempo_confe;
	private JTextField tb_ver_classi;
	private JTextField tb_ver_obser;
	private JTextField tb_ver_receita;
	private JTextField nomeIngrediente;
	private JTextField caloriasIngredientes;
	private JTextField precoIngredientes;
	private JTextField txt_apagarReceita;
	private JTextField txt_idIngrediente;
	private JTextField nome_editarIngrediente;
	private JTextField calorias_editarIngrediente;
	private JTextField preco_editarIngrediente;
	private JTextField txt_apagarIngrediente;
	private JTextField loja;
	private JTextField DataCompras;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu frame = new Menu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Menu() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 833, 498);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
			// Ingrediente Gravar
			JPanel GuardarIngrediente = new JPanel();
			GuardarIngrediente.setVisible(false);
			// Ver receita
			JPanel VerReceita = new JPanel();
			VerReceita.setBackground(Color.WHITE);
			VerReceita.setVisible(false);
			// Ingrediente Menu
			JPanel IngredienteMenu = new JPanel();
			IngredienteMenu.setVisible(false);
						
			// Ver Ingrediente
			JPanel VerIngridiente = new JPanel();
			VerIngridiente.setVisible(false);
			
			JPanel compras = new JPanel();
			compras.setVisible(false);
			compras.setBackground(Color.WHITE);
			compras.setBounds(112, 0, 705, 459);
			contentPane.add(compras);
			compras.setLayout(null);
			
			JComboBox cb_receita = new JComboBox();
			cb_receita.setBounds(499, 31, 157, 27);
			compras.add(cb_receita);
			
			JList list_ingredientes1 = new JList();
			list_ingredientes1.setBounds(21, 55, 157, 327);
			compras.add(list_ingredientes1);
			
			JButton btt_adicionarLista = new JButton(">");
			btt_adicionarLista.setBounds(199, 145, 41, 27);
			compras.add(btt_adicionarLista);
			
			JButton btt_RetirarLista = new JButton("<");
			btt_RetirarLista.setBounds(199, 183, 41, 27);
			compras.add(btt_RetirarLista);
			
			JList list_IngredientesCompras = new JList();
			list_IngredientesCompras.setBounds(258, 82, 164, 298);
			compras.add(list_IngredientesCompras);
			
			loja = new JTextField();
			loja.setBounds(499, 108, 164, 20);
			compras.add(loja);
			loja.setColumns(10);
			
			JLabel lblNewLabel_5 = new JLabel("Loja");
			lblNewLabel_5.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			lblNewLabel_5.setBounds(499, 92, 46, 14);
			compras.add(lblNewLabel_5);
			
			JLabel lblNewLabel_6 = new JLabel("Data");
			lblNewLabel_6.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			lblNewLabel_6.setBounds(499, 145, 46, 14);
			compras.add(lblNewLabel_6);
			
			DataCompras = new JTextField();
			DataCompras.setBounds(499, 170, 164, 20);
			compras.add(DataCompras);
			DataCompras.setColumns(10);
			
			JButton btt_carregarElementosCompras = new JButton("Carregar Elementos");
			btt_carregarElementosCompras.setBounds(355, 386, 132, 33);
			compras.add(btt_carregarElementosCompras);
			btt_carregarElementosCompras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Connection c = null;
					Statement stmt = null;
					try {
					  Class.forName("org.sqlite.JDBC");
					  c = DriverManager.getConnection("jdbc:sqlite:ReceitaDromo.db");
					  c.setAutoCommit(false);
					  
					  ArrayList<receita> ListaIngredientes2 = new ArrayList<receita>();
					  
					  stmt = c.createStatement();
					  ResultSet rs = stmt.executeQuery( "SELECT * FROM Ingredientes;" );

					  
					  while ( rs.next() ) {
						  
					      String ingredienteslist = Integer.toString(rs.getInt("Id_ingredientes")) + " - " + rs.getString("Nome");
					      DefaultListModel dmI = new DefaultListModel();
					       dmI.addElement(ingredienteslist);
					       list_ingredientes1.setModel(dmI);
					  }
					  rs.close();
					  stmt.close();
					  c.close();
					} catch ( Exception e1 ) {
					  System.err.println( e1.getClass().getName() + ": " + e1.getMessage() );
					  System.exit(0);
					  
					}
					try {
						  Class.forName("org.sqlite.JDBC");
						  c = DriverManager.getConnection("jdbc:sqlite:ReceitaDromo.db");
						  c.setAutoCommit(false);
						  
						  ArrayList<receita> ListaIngredientes2 = new ArrayList<receita>();
						  
						  stmt = c.createStatement();
						  ResultSet rs = stmt.executeQuery( "SELECT * FROM Receita;" );

						  
						  while ( rs.next() ) {
							  
						      String ingredienteslist = Integer.toString(rs.getInt("Id_Receita")) + " - " + rs.getString("nome");
						      cb_receita.addItem(ingredienteslist);
						  }
						  rs.close();
						  stmt.close();
						  c.close();
						} catch ( Exception e1 ) {
						  System.err.println( e1.getClass().getName() + ": " + e1.getMessage() );
						  System.exit(0);
						  
						}
				}
				
				
				
			});
			
			JButton btt_gravarCompras = new JButton("Gravar Compras");
			btt_gravarCompras.setBounds(499, 386, 142, 33);
			compras.add(btt_gravarCompras);
			btt_carregarElementosCompras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					comprasclass.inserir_compras(DataCompras.getText(), loja.getText());
					
				}
				
				
				
			});
			VerIngridiente.setBackground(Color.WHITE);
			VerIngridiente.setBounds(112, 0, 705, 459);
			contentPane.add(VerIngridiente);
			VerIngridiente.setLayout(null);
			
			JList listIngredintes = new JList();
			listIngredintes.setBorder(new LineBorder(new Color(0, 0, 0)));
			listIngredintes.setBounds(175, 72, -148, 379);
			VerIngridiente.add(listIngredintes);
			
			JLabel lblIntroduzaONumero = new JLabel("Introduza o numero do ingrediente :");
			lblIntroduzaONumero.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			lblIntroduzaONumero.setBounds(50, 14, 226, 14);
			VerIngridiente.add(lblIntroduzaONumero);
			
			txt_idIngrediente = new JTextField();
			txt_idIngrediente.setColumns(10);
			txt_idIngrediente.setBounds(260, 12, 86, 20);
			VerIngridiente.add(txt_idIngrediente);
			
			JLabel label_8 = new JLabel("Nome :");
			label_8.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			label_8.setBounds(387, 48, 62, 17);
			VerIngridiente.add(label_8);
			
			JLabel lblCalorias_1 = new JLabel("Calorias :");
			lblCalorias_1.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			lblCalorias_1.setBounds(387, 83, 91, 17);
			VerIngridiente.add(lblCalorias_1);
			
			JLabel lblDescrio_1 = new JLabel("Descri\u00E7\u00E3o :");
			lblDescrio_1.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			lblDescrio_1.setBounds(387, 111, 115, 17);
			VerIngridiente.add(lblDescrio_1);
			
			JLabel lblPreo_1 = new JLabel("Pre\u00E7o :");
			lblPreo_1.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			lblPreo_1.setBounds(387, 195, 128, 17);
			VerIngridiente.add(lblPreo_1);
			
			JButton btt_procurarIngridiente = new JButton("Procurar");
			btt_procurarIngridiente.setBounds(356, 11, 89, 23);
			VerIngridiente.add(btt_procurarIngridiente);
			btt_procurarIngridiente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Connection c = null;
					Statement stmt = null;
					try {
					  Class.forName("org.sqlite.JDBC");
					  c = DriverManager.getConnection("jdbc:sqlite:ReceitaDromo.db");
					  c.setAutoCommit(false);
					  
					  ArrayList<receita> ListaIngredientes2 = new ArrayList<receita>();
					  
					  stmt = c.createStatement();
					  ResultSet rs = stmt.executeQuery( "SELECT * FROM Ingredientes=" +Integer.parseInt(txt_idIngrediente.getText()) + ";" );

					  
					  while ( rs.next() ) {
						  
					      String ingredienteslist = rs.getInt("Id_ingredientes") + " - " + rs.getString("Nome");
					      DefaultListModel dmI = new DefaultListModel();
					       dmI.addElement(ingredienteslist);
					       listIngredintes.setModel(dmI);
					  }
					  rs.close();
					  stmt.close();
					  c.close();
					} catch ( Exception e1 ) {
					  System.err.println( e1.getClass().getName() + ": " + e1.getMessage() );
					  System.exit(0);
					  
					}
				}
			});
			nome_editarIngrediente = new JTextField();
			nome_editarIngrediente.setEditable(false);
			nome_editarIngrediente.setColumns(10);
			nome_editarIngrediente.setBounds(471, 45, 145, 20);
			VerIngridiente.add(nome_editarIngrediente);
			
			calorias_editarIngrediente = new JTextField();
			calorias_editarIngrediente.setEditable(false);
			calorias_editarIngrediente.setColumns(10);
			calorias_editarIngrediente.setBounds(471, 80, 145, 20);
			VerIngridiente.add(calorias_editarIngrediente);
			
			preco_editarIngrediente = new JTextField();
			preco_editarIngrediente.setEditable(false);
			preco_editarIngrediente.setColumns(10);
			preco_editarIngrediente.setBounds(471, 192, 145, 20);
			VerIngridiente.add(preco_editarIngrediente);
			// Aqui para carregar os elementos Ingridentes
			JButton btt_carregarIngredientes = new JButton("Carregar Elementos");
			btt_carregarIngredientes.setBounds(273, 288, 133, 38);
			VerIngridiente.add(btt_carregarIngredientes);
			btt_carregarIngredientes.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Connection c = null;
					Statement stmt = null;
					try {
					  Class.forName("org.sqlite.JDBC");
					  c = DriverManager.getConnection("jdbc:sqlite:ReceitaDromo.db");
					  c.setAutoCommit(false);
					  
					  ArrayList<receita> ListaIngredientes2 = new ArrayList<receita>();
					  
					  stmt = c.createStatement();
					  ResultSet rs = stmt.executeQuery( "SELECT * FROM Ingredientes;" );

					  
					  while ( rs.next() ) {
						  
					      String ingredienteslist = rs.getInt("Id_ingredientes") + " - " + rs.getString("Nome");
					      DefaultListModel dmI = new DefaultListModel();
					       dmI.addElement(ingredienteslist);
					       listIngredintes.setModel(dmI);
					  }
					  rs.close();
					  stmt.close();
					  c.close();
					} catch ( Exception e1 ) {
					  System.err.println( e1.getClass().getName() + ": " + e1.getMessage() );
					  System.exit(0);
					  
					}
				}
				
				
			});
			
			JButton btt_ApagarIngrediente = new JButton("Apagar Ingrediente");
			btt_ApagarIngrediente.setBounds(414, 288, 133, 38);
			VerIngridiente.add(btt_ApagarIngrediente);
			btt_carregarIngredientes.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				Ingrediente.deleteDB(Integer.parseInt(txt_apagarIngrediente.getText()));	
				}
			});
			
			txt_apagarIngrediente = new JTextField();
			txt_apagarIngrediente.setColumns(10);
			txt_apagarIngrediente.setBounds(470, 337, 86, 20);
			VerIngridiente.add(txt_apagarIngrediente);
			
			JLabel label_15 = new JLabel("Introduza o id que deseja apagar :");
			label_15.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			label_15.setBounds(273, 340, 199, 14);
			VerIngridiente.add(label_15);
			
			JEditorPane descricao_editarIngrediente = new JEditorPane();
			descricao_editarIngrediente.setEnabled(false);
			descricao_editarIngrediente.setBounds(471, 108, 145, 77);
			VerIngridiente.add(descricao_editarIngrediente);
			
			JButton btt_editarIngrediente = new JButton("Editar Ingrediente");
			btt_editarIngrediente.setEnabled(false);
			btt_editarIngrediente.setBounds(555, 288, 132, 38);
			VerIngridiente.add(btt_editarIngrediente);
			btt_editarIngrediente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int Idingredientes = Integer.parseInt(txt_idIngrediente.getText());
					String Nome = nome_editarIngrediente.getText();
					int calorias = Integer.parseInt(calorias_editarIngrediente.getText());
					String descricao = descricao_editarIngrediente.getText();
					float preco = Float.parseFloat(preco_editarIngrediente.getText());
					
					Ingrediente.updateDB(Idingredientes, Nome, calorias, descricao, preco);
					
				}
				
				
			});
			
			
			JCheckBox checkb_editarIngrediente = new JCheckBox("Editar Ingrediente?");
			checkb_editarIngrediente.setBounds(559, 245, 128, 23);
			VerIngridiente.add(checkb_editarIngrediente);
			checkb_editarIngrediente.addItemListener(new ItemListener() {
			    
				@Override
			    public void itemStateChanged(ItemEvent e) {
			        if(e.getStateChange() == ItemEvent.SELECTED) {//checkbox has been selected
			           nome_editarIngrediente.setEditable(true);
			           calorias_editarIngrediente.setEditable(true);
			           preco_editarIngrediente.setEditable(true);
			           descricao_editarIngrediente.setEditable(true);
			           descricao_editarIngrediente.setEnabled(true);
			           btt_editarIngrediente.setEnabled(true);
			           
			           
			           
			        } else {//checkbox has been deselected
			        	 nome_editarIngrediente.setEditable(false);
				           calorias_editarIngrediente.setEditable(false);
				           preco_editarIngrediente.setEditable(false);
				           descricao_editarIngrediente.setEditable(false);
				           descricao_editarIngrediente.setEnabled(false);
				           btt_editarIngrediente.setEnabled(false);
				           
			        };
			    }
			});
			
			
			
			
			
	
			
			
	
			// Receita Menu
						JPanel receitap = new JPanel();
						receitap.setBackground(Color.WHITE);
						receitap.setBounds(112, 0, 705, 459);
						contentPane.add(receitap);
						receitap.setLayout(null);
			
			// Guardar Receita
						JPanel GuardarReceita = new JPanel();
						GuardarReceita.setVisible(false);
						GuardarReceita.setBackground(Color.WHITE);
						GuardarReceita.setBounds(112, 0, 705, 459);
						contentPane.add(GuardarReceita);
						GuardarReceita.setLayout(null);
			IngredienteMenu.setBackground(Color.WHITE);
			IngredienteMenu.setBounds(112, 0, 705, 459);
			contentPane.add(IngredienteMenu);
			IngredienteMenu.setLayout(null);
			
			
			JButton bttGuardarIngrediente = new JButton("Guardar Ingredinte");
			bttGuardarIngrediente.setBounds(150, 143, 125, 70);
			IngredienteMenu.add(bttGuardarIngrediente);
			bttGuardarIngrediente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					receitap.setVisible(false);
					GuardarReceita.setVisible(false);
					VerReceita.setVisible(false);
					VerIngridiente.setVisible(false);
					IngredienteMenu.setVisible(false);
					GuardarIngrediente.setVisible(true);
					compras.setVisible(false);
				}
				
				
			});
			JButton bttVerReceita = new JButton("Ver Receita");
			bttVerReceita.setBounds(409, 143, 125, 70);
			IngredienteMenu.add(bttVerReceita);
			bttVerReceita.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					receitap.setVisible(false);
					GuardarReceita.setVisible(false);
					VerReceita.setVisible(false);
					VerIngridiente.setVisible(true);
					IngredienteMenu.setVisible(false);
					GuardarIngrediente.setVisible(false);
					compras.setVisible(false);
				}
				
				
			});
	
			
			
			JButton verReceita = new JButton("Ver Receita");
			verReceita.setBounds(182, 149, 121, 81);
			receitap.add(verReceita);
			verReceita.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					receitap.setVisible(false);
					GuardarReceita.setVisible(false);
					VerReceita.setVisible(true);
					VerIngridiente.setVisible(false);
					IngredienteMenu.setVisible(false);
					GuardarIngrediente.setVisible(false);
					compras.setVisible(false);
				}
				
				
			});
			
			JButton addReceita = new JButton("Adicionar Receita");
			addReceita.setBounds(404, 149, 121, 81);
			receitap.add(addReceita);
			addReceita.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					receitap.setVisible(false);
					GuardarReceita.setVisible(true);
					VerReceita.setVisible(false);
					VerIngridiente.setVisible(false);
					IngredienteMenu.setVisible(false);
					GuardarIngrediente.setVisible(false);
					compras.setVisible(false);
				}
				
				
			});
			VerReceita.setBounds(112, 0, 705, 459);
			contentPane.add(VerReceita);
			VerReceita.setLayout(null);
			
			JList list = new JList();
			list.setBorder(new LineBorder(new Color(0, 0, 0)));
			list.setBounds(151, 60, -129, 388);
			VerReceita.add(list);
			
					
					JLabel lblNewLabel_2 = new JLabel("Introduza o numero da receita :");
					lblNewLabel_2.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
					lblNewLabel_2.setBounds(71, 11, 226, 14);
					VerReceita.add(lblNewLabel_2);
					
					txt_idreceita = new JTextField();
					txt_idreceita.setBounds(255, 9, 86, 20);
					VerReceita.add(txt_idreceita);
					txt_idreceita.setColumns(10);
					
					JLabel label = new JLabel("Nome :");
					label.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
					label.setBounds(339, 41, 62, 17);
					VerReceita.add(label);
					
					JLabel label_1 = new JLabel("N\u00BA de Doses :");
					label_1.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
					label_1.setBounds(339, 76, 91, 17);
					VerReceita.add(label_1);
					
					JLabel label_2 = new JLabel("Tempo Prepara\u00E7\u00E3o :");
					label_2.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
					label_2.setBounds(339, 104, 115, 17);
					VerReceita.add(label_2);
					
					JLabel label_3 = new JLabel("Tempo confeca\u00E7\u00E3o :");
					label_3.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
					label_3.setBounds(339, 132, 128, 17);
					VerReceita.add(label_3);
					
					JLabel label_4 = new JLabel("Classifica\u00E7\u00E3o :");
					label_4.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
					label_4.setBounds(339, 160, 115, 17);
					VerReceita.add(label_4);
					
					JLabel label_5 = new JLabel("Observa\u00E7\u00F5es :");
					label_5.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
					label_5.setBounds(339, 188, 115, 17);
					VerReceita.add(label_5);
					
					JLabel label_6 = new JLabel("Tipo Receita :");
					label_6.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
					label_6.setBounds(339, 216, 115, 17);
					VerReceita.add(label_6);
					
					JButton btt_ProcurarReceita = new JButton("Procurar");
					btt_ProcurarReceita.setBounds(351, 8, 89, 23);
					VerReceita.add(btt_ProcurarReceita);
					addReceita.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
				
						}
						
						
					});
					
					
					tb_ver_nome = new JTextField();
					tb_ver_nome.setEditable(false);
					tb_ver_nome.setColumns(10);
					tb_ver_nome.setBounds(465, 40, 145, 20);
					VerReceita.add(tb_ver_nome);
					
					tb_ver_n_doses = new JTextField();
					tb_ver_n_doses.setEditable(false);
					tb_ver_n_doses.setColumns(10);
					tb_ver_n_doses.setBounds(465, 75, 145, 20);
					VerReceita.add(tb_ver_n_doses);
					
					tb_ver_tempo_prepa = new JTextField();
					tb_ver_tempo_prepa.setEditable(false);
					tb_ver_tempo_prepa.setColumns(10);
					tb_ver_tempo_prepa.setBounds(465, 103, 145, 20);
					VerReceita.add(tb_ver_tempo_prepa);
					
					tb_ver_tempo_confe = new JTextField();
					tb_ver_tempo_confe.setEditable(false);
					tb_ver_tempo_confe.setColumns(10);
					tb_ver_tempo_confe.setBounds(465, 131, 145, 20);
					VerReceita.add(tb_ver_tempo_confe);
					
					tb_ver_classi = new JTextField();
					tb_ver_classi.setEditable(false);
					tb_ver_classi.setColumns(10);
					tb_ver_classi.setBounds(465, 160, 145, 20);
					VerReceita.add(tb_ver_classi);
					
					tb_ver_obser = new JTextField();
					tb_ver_obser.setEditable(false);
					tb_ver_obser.setColumns(10);
					tb_ver_obser.setBounds(465, 187, 145, 20);
					VerReceita.add(tb_ver_obser);
					
					tb_ver_receita = new JTextField();
					tb_ver_receita.setEditable(false);
					tb_ver_receita.setColumns(10);
					tb_ver_receita.setBounds(465, 215, 145, 20);
					VerReceita.add(tb_ver_receita);
					
					JButton btt_loadValues = new JButton("Carregar Elementos");
					btt_loadValues.setBounds(268, 285, 133, 38);
					VerReceita.add(btt_loadValues);
					btt_loadValues.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							Connection c = null;
							Statement stmt = null;
							try {
							  Class.forName("org.sqlite.JDBC");
							  c = DriverManager.getConnection("jdbc:sqlite:ReceitaDromo.db");
							  c.setAutoCommit(false);
							  
							  ArrayList<receita> Listareceita2 = new ArrayList<receita>();
							  
							  stmt = c.createStatement();
							  ResultSet rs = stmt.executeQuery( "SELECT * FROM Receita;" );

							  
							  while ( rs.next() ) {
								  
							      String receitalist = Integer.toString(rs.getInt("id_receita")) + " - " + rs.getString("nome");
							      DefaultListModel dm = new DefaultListModel();
							       dm.addElement(receitalist);
							       list.setModel(dm);
							  }
							  rs.close();
							  stmt.close();
							  c.close();
							} catch ( Exception e1 ) {
							  System.err.println( e1.getClass().getName() + ": " + e1.getMessage() );
							  System.exit(0);
							  
							}
						}
						
						
					});
					
					JButton btt_ApagarReceita = new JButton("Apagar Receita");
					btt_ApagarReceita.setBounds(409, 285, 133, 38);
					VerReceita.add(btt_ApagarReceita);
					btt_ApagarReceita.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
						
							receita.deleteDB(Integer.parseInt(txt_apagarReceita.getText()));
						}	
					});

					
					txt_apagarReceita = new JTextField();
					txt_apagarReceita.setBounds(465, 334, 86, 20);
					VerReceita.add(txt_apagarReceita);
					txt_apagarReceita.setColumns(10);
					
					JLabel lblNewLabel_4 = new JLabel("Introduza o id que deseja apagar :");
					lblNewLabel_4.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
					lblNewLabel_4.setBounds(268, 337, 199, 14);
					VerReceita.add(lblNewLabel_4);
					
					JButton btt_EditarReceita = new JButton("Editar Receita");
					btt_EditarReceita.setEnabled(false);
					btt_EditarReceita.setBounds(550, 285, 132, 38);
					VerReceita.add(btt_EditarReceita);
					btt_EditarReceita.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							int id_receita = Integer.parseInt(txt_idreceita.getText());
							String nome = tb_ver_nome.getText();
							int ndoses =Integer.parseInt(tb_ver_n_doses.getText()) ;
							int tempoPrepa = Integer.parseInt(tb_ver_tempo_prepa.getText());
							int tempoConfe = Integer.parseInt(tb_ver_tempo_confe.getText());
							int classi = Integer.parseInt(tb_ver_classi.getText());
							String obser = tb_ver_obser.getText();
							String tipo_receita = tb_ver_receita.getText();
							
							receita.updateDB(id_receita, nome, ndoses, tempoPrepa, tempoConfe, classi, obser, tipo_receita);
						}
						
						
					});
					JCheckBox checkb_EditarReceita = new JCheckBox("Editar receita?");
					checkb_EditarReceita.setBounds(585, 242, 97, 23);
					VerReceita.add(checkb_EditarReceita);
					checkb_EditarReceita.addItemListener(new ItemListener() {
					    
						@Override
					    public void itemStateChanged(ItemEvent e) {
					        if(e.getStateChange() == ItemEvent.SELECTED) {//checkbox has been selected
					           tb_ver_nome.setEditable(true);
					           tb_ver_n_doses.setEditable(true);
					           tb_ver_tempo_prepa.setEditable(true);
					           tb_ver_tempo_confe.setEditable(true);
					           tb_ver_classi.setEditable(true);
					           tb_ver_obser.setEditable(true);
					           tb_ver_receita.setEditable(true);
					           btt_EditarReceita.setEnabled(true);
					           
					           
					        } else {//checkbox has been deselected
					        	 tb_ver_nome.setEditable(false);
						           tb_ver_n_doses.setEditable(false);
						           tb_ver_tempo_prepa.setEditable(false);
						           tb_ver_tempo_confe.setEditable(false);
						           tb_ver_classi.setEditable(false);
						           tb_ver_obser.setEditable(false);
						           tb_ver_receita.setEditable(false);
						           btt_EditarReceita.setEnabled(false);
						           
					        };
					    }
					});
					
					
					
					
			GuardarIngrediente.setBackground(Color.WHITE);
			GuardarIngrediente.setBounds(112, 0, 705, 459);
			contentPane.add(GuardarIngrediente);
			GuardarIngrediente.setLayout(null);
			
			JLabel lblNewLabel_3 = new JLabel("Nome :");
			lblNewLabel_3.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			lblNewLabel_3.setBounds(25, 38, 52, 17);
			GuardarIngrediente.add(lblNewLabel_3);
			
			nomeIngrediente = new JTextField();
			nomeIngrediente.setBounds(94, 37, 125, 19);
			GuardarIngrediente.add(nomeIngrediente);
			nomeIngrediente.setColumns(10);
			
			JLabel lblCalorias = new JLabel("Calorias :");
			lblCalorias.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			lblCalorias.setBounds(25, 71, 52, 17);
			GuardarIngrediente.add(lblCalorias);
			
			caloriasIngredientes = new JTextField();
			caloriasIngredientes.setColumns(10);
			caloriasIngredientes.setBounds(94, 68, 125, 19);
			GuardarIngrediente.add(caloriasIngredientes);
			
			JLabel lblDescrio = new JLabel("Descri\u00E7\u00E3o");
			lblDescrio.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			lblDescrio.setBounds(25, 108, 74, 17);
			GuardarIngrediente.add(lblDescrio);
			
			JLabel lblPreo = new JLabel("Pre\u00E7o :");
			lblPreo.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			lblPreo.setBounds(352, 38, 52, 17);
			GuardarIngrediente.add(lblPreo);
			
			precoIngredientes = new JTextField();
			precoIngredientes.setColumns(10);
			precoIngredientes.setBounds(414, 37, 125, 19);
			GuardarIngrediente.add(precoIngredientes);
			
			JEditorPane descricaoIngredientes = new JEditorPane();
			descricaoIngredientes.setText("Escreve Aqui...");
			descricaoIngredientes.setForeground(Color.BLACK);
			descricaoIngredientes.setBounds(25, 136, 201, 141);
			GuardarIngrediente.add(descricaoIngredientes);
			
			JButton btt_GravarIngrediente = new JButton("Gravar Ingrediente");
			btt_GravarIngrediente.setBounds(414, 284, 156, 40);
			GuardarIngrediente.add(btt_GravarIngrediente);
			btt_GravarIngrediente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						String nome = nomeIngrediente.getText();
						int calorias = 0;
						String descricao = descricaoIngredientes.getText();
						float preco = 0;
						
						try {
							calorias = Integer.parseInt(caloriasIngredientes.getText());
						} catch (NumberFormatException e1) {
						
						}
						try {
							preco = Float.parseFloat(precoIngredientes.getText());
						} catch (NumberFormatException e1) {
							
						}						
						
						Ingrediente.inserir_Ingredientes(nome, calorias, descricao, preco);
					}
					catch(Exception e1) {
						
						
					}
					
			
				}
				
				
			});
			
			
			
			
			JButton Ingrediente = new JButton("Ingrediente");
			Ingrediente.setBounds(10, 60, 96, 38);
			contentPane.add(Ingrediente);
			Ingrediente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					receitap.setVisible(false);
					GuardarReceita.setVisible(false);
					VerReceita.setVisible(false);
					VerIngridiente.setVisible(false);
					IngredienteMenu.setVisible(true);
					GuardarIngrediente.setVisible(false);
					compras.setVisible(false);
				}
				
				
			});
			
			JButton Compras = new JButton("Compras");
			Compras.setBounds(10, 109, 96, 38);
			contentPane.add(Compras);
			Compras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					receitap.setVisible(false);
					GuardarReceita.setVisible(false);
					VerReceita.setVisible(false);
					VerIngridiente.setVisible(false);
					IngredienteMenu.setVisible(false);
					GuardarIngrediente.setVisible(false);
					compras.setVisible(true);
				}
				
				
			});
			
			
			JButton Receita = new JButton("Receita");
			Receita.setBounds(10, 11, 96, 38);
			contentPane.add(Receita);
			Receita.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					receitap.setVisible(true);
					GuardarReceita.setVisible(false);
					VerReceita.setVisible(false);
					VerIngridiente.setVisible(false);
					IngredienteMenu.setVisible(false);
					GuardarIngrediente.setVisible(false);
					compras.setVisible(false);
				}
				
				
			});
			
			
				
				
				
			
		
		
		
		
	
		
		
		
		
		
		
		JLabel lblNewLabel = new JLabel("Nome :");
		lblNewLabel.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
		lblNewLabel.setBounds(10, 28, 62, 17);
		GuardarReceita.add(lblNewLabel);
		
		JLabel lblNDeDoses = new JLabel("N\u00BA de Doses :");
		lblNDeDoses.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
		lblNDeDoses.setBounds(10, 63, 91, 17);
		GuardarReceita.add(lblNDeDoses);
		
		JLabel lblTempoPreparao = new JLabel("Tempo Prepara\u00E7\u00E3o :");
		lblTempoPreparao.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
		lblTempoPreparao.setBounds(10, 91, 115, 17);
		GuardarReceita.add(lblTempoPreparao);
		
		JLabel lblTempoConfecao = new JLabel("Tempo confeca\u00E7\u00E3o :");
		lblTempoConfecao.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
		lblTempoConfecao.setBounds(10, 119, 128, 17);
		GuardarReceita.add(lblTempoConfecao);
		
		JLabel lblClassificao = new JLabel("Classifica\u00E7\u00E3o :");
		lblClassificao.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
		lblClassificao.setBounds(10, 147, 115, 17);
		GuardarReceita.add(lblClassificao);
		
		JLabel lblObservaes = new JLabel("Observa\u00E7\u00F5es :");
		lblObservaes.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
		lblObservaes.setBounds(10, 175, 115, 17);
		GuardarReceita.add(lblObservaes);
		
		JLabel lblTipoReceita = new JLabel("Tipo Receita :");
		lblTipoReceita.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
		lblTipoReceita.setBounds(10, 203, 115, 17);
		GuardarReceita.add(lblTipoReceita);
		
		tb_nome = new JTextField();
		tb_nome.setBounds(136, 27, 145, 20);
		GuardarReceita.add(tb_nome);
		tb_nome.setColumns(10);
		
		tb_n_doses = new JTextField();
		tb_n_doses.setColumns(10);
		tb_n_doses.setBounds(136, 62, 145, 20);
		GuardarReceita.add(tb_n_doses);
		
		tb_tempo_prep = new JTextField();
		tb_tempo_prep.setColumns(10);
		tb_tempo_prep.setBounds(136, 90, 145, 20);
		GuardarReceita.add(tb_tempo_prep);
		
		tb_tempo_confe = new JTextField();
		tb_tempo_confe.setColumns(10);
		tb_tempo_confe.setBounds(136, 118, 145, 20);
		GuardarReceita.add(tb_tempo_confe);
		
		tb_classi = new JTextField();
		tb_classi.setColumns(10);
		tb_classi.setBounds(136, 147, 145, 20);
		GuardarReceita.add(tb_classi);
		
		tb_observ = new JTextField();
		tb_observ.setColumns(10);
		tb_observ.setBounds(136, 174, 145, 20);
		GuardarReceita.add(tb_observ);
		
		tb_tipo_receita = new JTextField();
		tb_tipo_receita.setColumns(10);
		tb_tipo_receita.setBounds(136, 202, 145, 20);
		GuardarReceita.add(tb_tipo_receita);
		
		JLabel lblNewLabel_1 = new JLabel("* Escolha de 1-5");
		lblNewLabel_1.setBounds(297, 149, 91, 14);
		GuardarReceita.add(lblNewLabel_1);
		
		JButton btt_gravar_receita = new JButton("Gravar Receita");
		btt_gravar_receita.setBounds(333, 261, 115, 32);
		GuardarReceita.add(btt_gravar_receita);
		btt_gravar_receita.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String nome = tb_nome.getText();
					int n_doses = 0;
					int temp_prepa = 0;
					int tempo_confe = 0;
					int classi = 0;
					try {
						n_doses = Integer.parseInt(tb_n_doses.getText());
					} catch (NumberFormatException e1) {
					
					}
					try {
						temp_prepa = Integer.parseInt(tb_tempo_prep.getText());
					} catch (NumberFormatException e1) {
						
					}
					try {
						 tempo_confe = Integer.parseInt(tb_tempo_confe.getText());
					} catch (NumberFormatException e1) {
					
					}
					try {
						classi = Integer.parseInt(tb_classi.getText());
					} catch (NumberFormatException e1) {
					
					}
					String obser =  tb_observ.getText();
					String tipo_receita = tb_tipo_receita.getText();
					receita.inserir_receita(nome,n_doses,temp_prepa,tempo_confe , classi , obser , tipo_receita);
				}
				catch(Exception e1) {
					
					
				}
				
			}
			
			
		});
		
	}
}
