import java.awt.Cursor;
import java.awt.List;
import java.sql.*;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.sqlite.SQLiteDataSource;
public class receita {
		
		int id_receita;
		String nome;
		int n_doses;
		int tempo_prep;
		int tempo_conf;
		int classificacao;
		String observ;
		String tipo_receita;
		
	  
	   
	  public receita(int id_receita, String nome, int n_doses, int tempo_prep, int tempo_conf, int classificacao,
				String observ, String tipo_receita) {
			super();
			this.id_receita = id_receita;
			this.nome = nome;
			this.n_doses = n_doses;
			this.tempo_prep = tempo_prep;
			this.tempo_conf = tempo_conf;
			this.classificacao = classificacao;
			this.observ = observ;
			this.tipo_receita = tipo_receita;
		}

	public static void connectDB()
	  {
	      Connection c = null;
	        try {
	          Class.forName("org.sqlite.JDBC");
	          c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\tiago\\Desktop\\workspace\\ReceitaDromo\\ReceitaDromo.db");
	        } catch ( Exception e ) {
	          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	          System.exit(0);
	        }
	        
	  }
	   
	  public static void inserir_receita(String nome, int n_doses, int tempo_prep, int tempo_conf, int classificacao,
				String observ, String tipo_receita)
	  {
	      	Connection c = null;
	        Statement stmt = null;
	        try {
	          Class.forName("org.sqlite.JDBC");
	          c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\tiago\\Desktop\\workspace\\ReceitaDromo\\ReceitaDromo.db");
	          c.setAutoCommit(false);
	          
	 
	          stmt = c.createStatement();
	          stmt.executeUpdate("INSERT INTO `Receita`(`nome`, `n_doses`, `tempo_preparacao`, `tempo_confecacao`, `classificacao_utilizador`, `observacoes_utilizador`, `link_foto`, `tipo_receita`) "
	        		  				+ "VALUES ('"+ nome + "',"+ n_doses + ", " + tempo_prep + " , " + tempo_conf + " , " + classificacao + " , '" + observ + "' , '"+ "Nada"+ "' , '" + tipo_receita + "')");
	          
	          stmt.close();
	          c.commit();
	          c.close();
	        } catch ( Exception e ) {
	        JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());	          
	        
	        }
	        
	        JOptionPane.showMessageDialog(null, "Gravado sucesso");
	  }
	
	   
	
	  
	  
	  public static void updateDB(int id_receita, String nome, int ndoses, int tempoPrepa, int tempoConfe, int classi, String obser, String tipo_receita)
	  {
	    Connection c = null;
	    Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:ReceitaDromo.db");
	      c.setAutoCommit(false);
	      System.out.println("Opened database successfully");
	 
	      stmt = c.createStatement();
	      String sql = "UPDATE `Receita` SET `nome`='" + nome  +"',`n_doses`=" + ndoses +",`tempo_preparacao`=" + tempoPrepa + ","
	      		+ "`tempo_confecacao`=" + tempoConfe + ",`classificacao_utilizador`=" + classi +",`observacoes_utilizador`='" + obser + "',`tipo_receita`='"+ tipo_receita +"' WHERE `id_receita`=" +id_receita ;
	      stmt.executeUpdate(sql);
	      c.commit();
	 
	    
	      stmt.close();
	      c.close();
	    } catch ( Exception e ) {
	    JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());	
	    
	    }
	    JOptionPane.showMessageDialog(null, "Editado Com Sucesso");
	  }
	   
	  public static void deleteDB(int id_receita)
	  {
	      Connection c = null;
	        Statement stmt = null;
	        try {
	          Class.forName("org.sqlite.JDBC");
	          c = DriverManager.getConnection("jdbc:sqlite:ReceitaDromo.db");
	          c.setAutoCommit(false);
	          
	 
	          stmt = c.createStatement();
	          String sql = ("DELETE from Receita where id_receita=" + id_receita + ";");
	          stmt.executeUpdate(sql);
	          c.commit();
	         
	   
	          stmt.close();
	          c.close();
	        } catch ( Exception e ) {
	        JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());	
	          
	        }
	        JOptionPane.showMessageDialog(null, "Apagado Com Sucesso");
	  }
}
