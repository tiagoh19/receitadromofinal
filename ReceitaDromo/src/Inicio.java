import java.awt.BorderLayout;
import java.sql.*;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.AbstractListModel;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.JTextPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JEditorPane;

public class Inicio extends JFrame {

	private JPanel contentPane;
	private JTextField tb_nome;
	private JTextField tb_n_doses;
	private JTextField tb_tempo_confe;
	private JTextField tb_temp_prepa;
	private JTextField tb_tipo_receita;
	private JTextField tb_classi;
	private JTextField tb_duracao;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Inicio frame = new Inicio();
					frame.setVisible(true);
					 FrameDragListener frameDragListener = new FrameDragListener(frame);
		             frame.addMouseListener(frameDragListener);
		             frame.addMouseMotionListener(frameDragListener);

					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Inicio() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		setUndecorated(true);
		setBounds(100, 100, 867, 516);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(100, 149, 237));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		// pn_receita inicio
		JPanel pn_receita = new JPanel();
		pn_receita.setBackground(Color.WHITE);
		pn_receita.setBounds(182, 43, 685, 473);
		contentPane.add(pn_receita);
		pn_receita.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome ");
		lblNome.setFont(new Font("Arial", Font.PLAIN, 14));
		lblNome.setBounds(27, 27, 46, 14);
		pn_receita.add(lblNome);
		
		JLabel lblNDoses = new JLabel("N\u00BA Doses");
		lblNDoses.setFont(new Font("Arial", Font.PLAIN, 14));
		lblNDoses.setBounds(27, 52, 82, 14);
		pn_receita.add(lblNDoses);
		
		tb_nome = new JTextField();
		tb_nome.setBounds(171, 25, 86, 20);
		pn_receita.add(tb_nome);
		tb_nome.setColumns(10);
		
		tb_n_doses = new JTextField();
		tb_n_doses.setColumns(10);
		tb_n_doses.setBounds(171, 50, 86, 20);
		pn_receita.add(tb_n_doses);
		
		JLabel lblTempoPreparao = new JLabel("Tempo Prepara\u00E7\u00E3o");
		lblTempoPreparao.setFont(new Font("Arial", Font.PLAIN, 14));
		lblTempoPreparao.setBounds(27, 88, 134, 14);
		pn_receita.add(lblTempoPreparao);
		
		JLabel lblTempoDeConfeo = new JLabel("Tempo de confe\u00E7\u00E3o");
		lblTempoDeConfeo.setFont(new Font("Arial", Font.PLAIN, 14));
		lblTempoDeConfeo.setBounds(27, 113, 126, 14);
		pn_receita.add(lblTempoDeConfeo);
		
		tb_tempo_confe = new JTextField();
		tb_tempo_confe.setColumns(10);
		tb_tempo_confe.setBounds(171, 111, 86, 20);
		pn_receita.add(tb_tempo_confe);
		
		tb_temp_prepa = new JTextField();
		tb_temp_prepa.setColumns(10);
		tb_temp_prepa.setBounds(171, 86, 86, 20);
		pn_receita.add(tb_temp_prepa);
		
		JLabel lblTipoDeReceita = new JLabel("Tipo de receita");
		lblTipoDeReceita.setFont(new Font("Arial", Font.PLAIN, 14));
		lblTipoDeReceita.setBounds(27, 140, 126, 14);
		pn_receita.add(lblTipoDeReceita);
		
		tb_tipo_receita = new JTextField();
		tb_tipo_receita.setColumns(10);
		tb_tipo_receita.setBounds(171, 138, 86, 20);
		pn_receita.add(tb_tipo_receita);
		
		JLabel lblObservaes = new JLabel("Observa\u00E7\u00F5es");
		lblObservaes.setFont(new Font("Arial", Font.PLAIN, 14));
		lblObservaes.setBounds(27, 221, 96, 14);
		pn_receita.add(lblObservaes);
		
		JEditorPane txt_observ = new JEditorPane();
		txt_observ.setText("Aqui....");
		txt_observ.setBounds(17, 246, 157, 91);
		pn_receita.add(txt_observ);
		
		JLabel lblNewLabel_1 = new JLabel("Classifica\u00E7\u00E3o");
		lblNewLabel_1.setFont(new Font("Arial", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(17, 364, 106, 14);
		pn_receita.add(lblNewLabel_1);
		
		tb_classi = new JTextField();
		tb_classi.setBounds(126, 362, 120, 20);
		pn_receita.add(tb_classi);
		tb_classi.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("*Selecione de 1-5");
		lblNewLabel_2.setFont(new Font("Arial", Font.PLAIN, 10));
		lblNewLabel_2.setBounds(264, 365, 96, 14);
		pn_receita.add(lblNewLabel_2);
		
		JLabel lblEtapas = new JLabel("Etapas");
		lblEtapas.setBounds(431, 28, 46, 14);
		pn_receita.add(lblEtapas);
		
		JEditorPane txt_etapas = new JEditorPane();
		txt_etapas.setText("Aqui...");
		txt_etapas.setBounds(431, 52, 212, 127);
		pn_receita.add(txt_etapas);
		
		JLabel lblDurao = new JLabel("Dura\u00E7\u00E3o");
		lblDurao.setFont(new Font("Arial", Font.PLAIN, 14));
		lblDurao.setBounds(431, 190, 106, 14);
		pn_receita.add(lblDurao);
		
		tb_duracao = new JTextField();
		tb_duracao.setBounds(492, 190, 134, 20);
		pn_receita.add(tb_duracao);
		tb_duracao.setColumns(10);
		
		JButton btngravar_receita = new JButton("New button");
		btngravar_receita.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try {
				String nome = tb_nome.toString();
				int n_doses = 0;
				int temp_prepa = 0;
				int tempo_confe = 0;
				int classi = 0;
				try {
					n_doses = Integer.parseInt(tb_n_doses.toString());
				} catch (NumberFormatException e) {
				
				}
				try {
					temp_prepa = Integer.parseInt(tb_temp_prepa.toString());
				} catch (NumberFormatException e) {
					
				}
				try {
					 tempo_confe = Integer.parseInt(tb_tempo_confe.toString());
				} catch (NumberFormatException e) {
				
				}
				try {
					classi = Integer.parseInt(tb_classi.toString());
				} catch (NumberFormatException e) {
				
				}
				String obser =  txt_observ.toString();
				String tipo_receita = tb_tipo_receita.toString();
				receita.inserir_receita(nome,n_doses,temp_prepa,tempo_confe , classi , obser , tipo_receita);
				}
				catch (NumberFormatException e) {
					
				}
			}
			});
		btngravar_receita.setBounds(554, 407, 89, 23);
		pn_receita.add(btngravar_receita);
		
		JPanel home = new JPanel();
		home.setBounds(182, 43, 685, 473);
		home.setBackground(Color.WHITE);
		contentPane.add(home);
		home.setLayout(null);
		
		JList list_ingredientes = new JList();
		list_ingredientes.setBounds(382, 33, 247, 184);
		list_ingredientes.setBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 128), new Color(169, 169, 169), new Color(169, 169, 169)));
		home.add(list_ingredientes);
		
		JTextPane passos_preparação = new JTextPane();
		passos_preparação.setBounds(379, 232, 250, 205);
		passos_preparação.setEditable(false);
		passos_preparação.setBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 128), new Color(169, 169, 169), new Color(169, 169, 169)));
		home.add(passos_preparação);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(33, 57, 210, 36);
		home.add(comboBox);
		//Fim receita
		
		//Inicio ingredients
		JPanel pn_ingredientes = new JPanel();
		pn_ingredientes.setBackground(Color.WHITE);
		pn_ingredientes.setBounds(182, 43, 685, 473);
		contentPane.add(pn_ingredientes);
		pn_ingredientes.setLayout(null);
		
		JLabel lblNewLabel_3 = new JLabel("Nome");
		lblNewLabel_3.setFont(new Font("Arial", Font.PLAIN, 14));
		lblNewLabel_3.setBounds(26, 27, 49, 14);
		pn_ingredientes.add(lblNewLabel_3);
		
		textField_7 = new JTextField();
		textField_7.setBounds(85, 25, 99, 20);
		pn_ingredientes.add(textField_7);
		textField_7.setColumns(10);
		
		JLabel lblCalorias = new JLabel("Calorias");
		lblCalorias.setFont(new Font("Arial", Font.PLAIN, 14));
		lblCalorias.setBounds(26, 58, 64, 14);
		pn_ingredientes.add(lblCalorias);
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		textField_8.setBounds(85, 56, 99, 20);
		pn_ingredientes.add(textField_8);
		
		JLabel lblPreo = new JLabel("Pre\u00E7o");
		lblPreo.setFont(new Font("Arial", Font.PLAIN, 14));
		lblPreo.setBounds(26, 89, 49, 14);
		pn_ingredientes.add(lblPreo);
		
		textField_9 = new JTextField();
		textField_9.setColumns(10);
		textField_9.setBounds(85, 87, 99, 20);
		pn_ingredientes.add(textField_9);
		
		JEditorPane dtrpnAqui_2 = new JEditorPane();
		dtrpnAqui_2.setText("Aqui...");
		dtrpnAqui_2.setFont(new Font("Arial", Font.PLAIN, 12));
		dtrpnAqui_2.setBounds(26, 174, 148, 125);
		pn_ingredientes.add(dtrpnAqui_2);
		
		JLabel lblDescrio = new JLabel("Descri\u00E7\u00E3o");
		lblDescrio.setFont(new Font("Arial", Font.PLAIN, 14));
		lblDescrio.setBounds(26, 149, 76, 14);
		pn_ingredientes.add(lblDescrio);
		
		JButton btngravar_ingridientes = new JButton("New button");
		btngravar_ingridientes.setBounds(549, 382, 55, 54);
		pn_ingredientes.add(btngravar_ingridientes);
		// fim ingredients
		JButton Home = new JButton("Home");
		Home.setBounds(0, 43, 181, 57);
		Home.setForeground(Color.WHITE);
		Home.setBackground(new Color(255, 255, 255));
		Home.setBorderPainted(false);
		Home.setFocusPainted(false);
		Home.setOpaque(false);
		try {
		    Image img = ImageIO.read(getClass().getResource("resources/Home.png"));
		    Image newimg = img.getScaledInstance( 29, 21,  java.awt.Image.SCALE_SMOOTH ) ;
		    Home.setIcon(new ImageIcon(newimg));
		    
		    
		    
		  } catch (Exception ex) {
		    System.out.println(ex);
		  }
		Home.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e)
		    {
		    	pn_receita.setVisible(false);
		    	home.setVisible(true);
		    	
		    }
		});
		contentPane.add(Home);
		
		JButton Receita = new JButton("Receita");
		Receita.setBounds(0, 111, 181, 57);
		Receita.setForeground(new Color(255, 255, 255));
		Receita.setBackground(new Color(100, 149, 237));
		Receita.setBorderPainted(false);
		Receita.setFocusPainted(false);
		Receita.setOpaque(false);
		try {
		    Image img = ImageIO.read(getClass().getResource("resources/plus.png"));
		    Image newimg = img.getScaledInstance( 29, 21,  java.awt.Image.SCALE_SMOOTH ) ;
		    Receita.setIcon(new ImageIcon(newimg));
		    
		    
		    
		  } catch (Exception ex) {
		    System.out.println(ex);
		  }
		Receita.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e)
		    {
		    	pn_receita.setVisible(true);
		    	home.setVisible(false);
		    	pn_ingredientes.setVisible(false);
		    }
		});
		
		contentPane.add(Receita);
		
		JButton novo_ingrediente = new JButton("Novo Ingrediente");
		novo_ingrediente.setBounds(0, 179, 181, 57);
		novo_ingrediente.setForeground(new Color(255, 255, 255));
		novo_ingrediente.setBackground(new Color(100, 149, 237));
		novo_ingrediente.setBorderPainted(false);
		novo_ingrediente.setFocusPainted(false);
		novo_ingrediente.setOpaque(false);
		Receita.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e)
		    {
		    	pn_receita.setVisible(false);
		    	home.setVisible(false);
		    	pn_ingredientes.setVisible(true);
		    	
		    }
		});
		contentPane.add(novo_ingrediente);
		
		JButton btn_exit = new JButton("");
		btn_exit.setBounds(820, 11, 37, 28);
		btn_exit.setBackground(new Color(100, 149, 237));
		try {
		    Image img = ImageIO.read(getClass().getResource("resources/exit.png"));
		    Image newimg = img.getScaledInstance( 29, 21,  java.awt.Image.SCALE_SMOOTH ) ;
		    btn_exit.setIcon(new ImageIcon(newimg));
		    btn_exit.setBorder(null);
		    btn_exit.setBorderPainted(false);
		    btn_exit.setFocusPainted(false);
		    btn_exit.setOpaque(false);
		    
		    
		  } catch (Exception ex) {
		    System.out.println(ex);
		  }
		btn_exit.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e)
		    {
		    	System.exit(0);
		    }
		});

		
		contentPane.add(btn_exit);
		
		JLabel lblNewLabel = new JLabel("Design and Coding");
		lblNewLabel.setBounds(20, 341, 152, 39);
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setFont(new Font("Bradley Hand ITC", Font.PLAIN, 18));
		contentPane.add(lblNewLabel);
		
		JLabel lblBy = new JLabel("By");
		lblBy.setBounds(76, 378, 37, 39);
		lblBy.setForeground(new Color(255, 255, 255));
		lblBy.setFont(new Font("Bradley Hand ITC", Font.PLAIN, 18));
		contentPane.add(lblBy);
		
		JLabel lblTiagoHenrique = new JLabel("Tiago Henrique");
		lblTiagoHenrique.setBounds(31, 416, 130, 39);
		lblTiagoHenrique.setForeground(Color.WHITE);
		lblTiagoHenrique.setFont(new Font("Bradley Hand ITC", Font.PLAIN, 18));
		contentPane.add(lblTiagoHenrique);
		
		
	}
}
