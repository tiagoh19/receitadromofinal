import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class Ingrediente {
	
	int id_ingredinte;
	String Nome;
	int calorias;
	String descricao;
	float preco;
	

	public Ingrediente(int id_ingredinte, String nome, int calorias, String descricao, float preco) {
		super();
		this.id_ingredinte = id_ingredinte;
		Nome = nome;
		this.calorias = calorias;
		this.descricao = descricao;
		this.preco = preco;
	}

	public static void connectDB()
	  {
	      Connection c = null;
	        try {
	          Class.forName("org.sqlite.JDBC");
	          c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\tiago\\Desktop\\workspace\\ReceitaDromo\\ReceitaDromo.db");
	        } catch ( Exception e ) {
	          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	          System.exit(0);
	        }
	        
	  }
	   
	  public static void inserir_Ingredientes(String nome, int calorias, String descricao, float preco)
				
	  {
	      	Connection c = null;
	        Statement stmt = null;
	        try {
	          Class.forName("org.sqlite.JDBC");
	          c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\tiago\\Desktop\\workspace\\ReceitaDromo\\ReceitaDromo.db");
	          c.setAutoCommit(false);
	          
	 
	          stmt = c.createStatement();
	          stmt.executeUpdate("INSERT INTO `Ingredientes`(`Nome`, `calorias`, `descricao`, `preco`) "
	        		  				+ "VALUES ('"+ nome + "',"+ calorias + ", '" + descricao + "' , " + preco + ")");
	          
	          stmt.close();
	          c.commit();
	          c.close();
	        } catch ( Exception e ) {
	          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	          System.exit(0);
	        }
	        
	        JOptionPane.showMessageDialog(null, "Gravado sucesso");
	  }
	  
	   
	
	  public static void updateDB(int idIngredientes, String Nome, int calorias, String descricao, float preco)
	  {
		  Connection c = null;
		    Statement stmt = null;
		    try {
		      Class.forName("org.sqlite.JDBC");
		      c = DriverManager.getConnection("jdbc:sqlite:ReceitaDromo.db");
		      c.setAutoCommit(false);
		      System.out.println("Opened database successfully");
		 
		      stmt = c.createStatement();
		      String sql = "UPDATE `Ingredientes` SET `Nome`='" + Nome  +"',`calorias`=" + calorias +",`descricao`='" + descricao + "',"
		      		+ "`preco`=" + preco + " WHERE `Id_ingredientes`=" +idIngredientes ;
		      stmt.executeUpdate(sql);
		      c.commit();
		 
		    
		      stmt.close();
		      c.close();
		    } catch ( Exception e ) {
		    JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());	
		    
		    }
		    JOptionPane.showMessageDialog(null, "Editado Com Sucesso");
		  }
	   
	  public static void deleteDB(int idIngredinte)
	  {
	      Connection c = null;
	        Statement stmt = null;
	        try {
	          Class.forName("org.sqlite.JDBC");
	          c = DriverManager.getConnection("jdbc:sqlite:ReceitaDromo.db");
	          c.setAutoCommit(false);
	         
	 
	          stmt = c.createStatement();
	          String sql = "DELETE from Ingredientes where Id_ingredientes=" + idIngredinte + ";";
	          stmt.executeUpdate(sql);
	          c.commit();
	 
	
	          stmt.close();
	          c.close();
	        } catch ( Exception e ) {
	        JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());	
	        
	        }
	        JOptionPane.showMessageDialog(null, "Apagado Com Sucesso");
	  }
}


